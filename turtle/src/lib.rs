use nannou::color::Alpha;
use nannou::Draw;
use nannou::prelude::*;

pub struct Turtle {
    position: Vec2,
    direction: f32,
    color: Alpha<Hsl, f32>,
    stack: Vec<(Vec2, f32, Alpha<Hsl, f32>)>,
}

impl Turtle {
    pub fn new() -> Self {
        Self {
            position: vec2(0., 0.),
            direction: 0.,
            color: hsla(0., 1., 1., 1.),
            stack: Vec::new(),
        }
    }

    pub fn position(mut self, position: Vec2) -> Self {
        self.position = position;

        self
    }

    pub fn direction(mut self, direction: f32) -> Self {
        self.direction = direction;

        self
    }

    pub fn direction_deg(mut self, degrees: f32) -> Self {
        self.direction = degrees * std::f32::consts::PI / 180.;

        self
    }

    pub fn color(mut self, color: Alpha<Hsl, f32>) -> Self {
        self.color = color;

        self
    }

    pub fn draw(&mut self, draw: &Draw, distance: f32) {
        let position = self.position + vec2(
            distance * self.direction.sin(),
            distance * self.direction.cos(),
        );
        draw.line()
            .color(self.color)
            .start(self.position)
            .end(position);
        self.position = position;
    }

    pub fn rotate(&mut self, radians: f32) {
        self.direction += radians;
    }

    pub fn rotate_deg(&mut self, degrees: f32) {
        self.rotate(degrees * std::f32::consts::PI / 180.)
    }

    pub fn push(&mut self) {
        self.stack.push((self.position, self.direction, self.color));
    }

    pub fn pop(&mut self) {
        match self.stack.pop() {
            Some(state) => {
                let (position, direction, color) = state;
                self.position = position;
                self.direction = direction;
                self.color = color;
            }
            None => {}
        }
    }
}