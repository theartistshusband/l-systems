use lindenmayer::LSystem;
use nannou::prelude::*;
use turtle::Turtle;

const WIDTH: u32 = 650;
const HEIGHT: u32 = 1000;

fn main() {
    nannou::app(model)
        .size(WIDTH, HEIGHT)
        .simple_window(view)
        .run();
}

struct Model {
    system: LSystem,
}


fn model(_app: &App) -> Model {
    let axiom = String::from("G");
    let rules = [
        ('F', "FF"),
        ('G', "F+[[G]-G]-F[-FG]+G")
    ];
    Model {
        system: LSystem::new(axiom, &rules),
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let mut turtle = Turtle::new()
        .direction_deg(25.)
        .position(vec2(45. - WIDTH as f32 / 2., 45. - HEIGHT as f32 / 2.))
        .color(hsla(0.3, 1., 0.5, 0.25));
    let draw = app.draw();

    let t = app.elapsed_frames() as usize;
    if t >= 4 && t <= 6 {
        for symbol in model.system.builder(t as usize) {
            match symbol {
                'F' => turtle.draw(&draw, 5.),
                '+' => turtle.rotate_deg(30.),
                '-' => turtle.rotate_deg(-20.),
                '[' => turtle.push(),
                ']' => turtle.pop(),
                _ => {}
            }
        }
    }
    draw.to_frame(app, &frame).unwrap()
}

