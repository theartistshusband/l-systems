use lindenmayer::LSystem;
use nannou::prelude::*;
use turtle::Turtle;

const WIDTH: u32 = 600;
const HEIGHT: u32 = 600;

fn main() {
    nannou::app(model)
        .size(WIDTH, HEIGHT)
        .simple_window(view)
        .run();
}

struct Model {
    system: LSystem,
}


fn model(_app: &App) -> Model {
    let axiom = String::from("F-G-G");
    let rules = [
        ('F', "F-G+F+G-F"),
        ('G', "GG")
    ];
    Model {
        system: LSystem::new(axiom, &rules),
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let mut turtle = Turtle::new()
        .direction_deg(90.)
        .position(vec2(45. - WIDTH as f32 / 2., 45. - HEIGHT as f32 / 2.))
        .color(hsla(1., 1., 0.5, 0.25));
    let draw = app.draw();

    let t = app.elapsed_frames() as usize;
    if t == 1 {
        draw.background().color(WHITE);
    }
    if t >= 1 && t <= 8 {
        for symbol in model.system.builder(t as usize) {
            match symbol {
                'F' => turtle.draw(&draw, 2.),
                'G' => turtle.draw(&draw, 2.),
                '+' => turtle.rotate_deg(120.),
                '-' => turtle.rotate_deg(-120.),
                _ => {}
            }
        }
    }
    draw.to_frame(app, &frame).unwrap()
}

